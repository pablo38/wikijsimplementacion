# Heroku Deploy for Wiki.js

This repo is an Heroku app definition for Wiki.js.  
For information about Wiki.js, check out the following links:

- [Official Website](https://wiki.js.org/)
- [GitHub Repository](https://github.com/Requarks/wiki)

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://github.com/requarks/wiki-heroku/tree/2.x) -> Este botón copiará el repositorio original desde GitHub, si se usa, luego será necesario eliminar el postgresql-addon y crear un addon con el plan que corresponda y versión 10 mediante heroku cli.

ej: $ heroku addons:create heroku-postgresql:hobby-dev --version=10

If you want to modify the configuration beyond what's available through environment variables, then:
* Clone this repo
* Make and commit your configuration changes
* Puede ser necesario crear una aplicación en heroku y luego trabajar sobre ella en heroku cli
* Antes de commitear en heroku cli indicar que será un container:
  $ heroku stack:set container
* `git remote add heroku https://git.heroku.com/my-wiki.git`
* `git push heroku`, or if you are on a branch, `git push heroku mybranch:master`
* Luego remover el addon de postgresql y crear uno con el plan que corresponda en versión 10.

ej: $ heroku addons:create heroku-postgresql:hobby-dev --version=10
